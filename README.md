# Calculation REST API

This is a Calculator REST API built with Spring Boot (version 2.7.8-SNAPSHOT). It supports the basic operations of
addition, subtraction, multiplication, and division, as well as parentheses for modifying the order of operations.

The API is implemented in Java 17 and includes comprehensive tests to ensure proper functionality.

The service supports 3 types of endpoints for calculating algorithmic expressions:

- /v1/calculate
    - This endpoint supports the basic operations of addition, subtraction, multiplication, and division. It does not
      support parentheses for modifying the order of operations.
- /v2/calculate
    - This endpoint supports the same operations as /v1/calculate, as well as parentheses for modifying the order of
      operations.
- /v3/calculate
    - This endpoint uses a JavaScript engine to evaluate expressions and supports all the features of /v2/calculate.
      It has access to the full library of functions and operators provided by the JavaScript engine.

# Create

To create a new calculation, send a POST request to the /calculate endpoint with the calculation data included in the
request body. The server will return a response with the result of the calculation.

The request should include a JSON object with a single field, "expression", which should contain the mathematical
expression you want to evaluate. The expression may include the following operators: +, -, *, /, and parentheses ( ) to
modify the order of operations.

For example, to create a new calculation to evaluate the expression "5*3/3+1", you might send a request like this:

```
POST http://localhost:8089/v1/calculate/
{
	"expression": "5*3/3+1"
}
```

If the calculation was performed successfully, the server might return a response like this:

```
{
    "id": "1994828438",
    "result": "6.0",
    "expression": "5*3/3+1",
    "error": ""
}
```

The calculator also rounds the result to two decimal places before returning it as a string.
For example, the expression "2/3" will return a result of 0.66, not 0.

# Read

To retrieve the result of a previous calculation, send a GET request to the /calculate/{id} endpoint, where {id} is the
ID of the calculation you want to retrieve. If the calculation exists, the server will return a response with the result
of the calculation. If the calculation does not exist, the server will return a 404 error.

For example, to retrieve the result of calculation with ID 1994828438, you might send a request like this:

```
GET http://localhost:8089/v1/calculate/1994828438
```

The server might return a response like this:

```
{
    "id": "1994828438",
    "result": "6.0",
    "expression": "5*3/3+1",
    "error": ""
}
```

To get all calculation results, you need to send a GET request to the /calculate/ endpoint.

```
GET http://localhost:8089/v1/calculate/
```

If a calculation exists, the server will return a response with the result of the calculation in an array.

# Update

To update the result of the desired calculation, send a PUT request to the /calculate/{id} endpoint with the calculation
data included in the request body, where {id} is the ID of the calculation you want to retrieve.

```
PUT http://localhost:8089/v1/calculate/1994828438
{
	"expression": "5*3/(3+1)"
}
```

If the calculation was performed successfully, the server might return a response like this:

```
{
    "id": "1783347379",
    "result": "3.75",
    "expression": "5*3/(3+1)",
    "error": ""
}
```

# Delete

It is not necessary to delete calculations, as they are typically ephemeral and do not need to be persisted. However, if
you wish to delete a calculation, you can send a DELETE request to the /calculate/{id} endpoint, where {id} is the ID of
the calculation you want to delete. The server will return a response indicating whether to delete was successful.

For example, to delete the calculation with ID 1994828438, you might send a request like this:

```
DELETE http://localhost:8089/v1/calculate/1994828438
```

If to delete was successful, the server might return a response like this:

```
{
    "message": "Calculation 1994828438 successfully deleted."
}
```