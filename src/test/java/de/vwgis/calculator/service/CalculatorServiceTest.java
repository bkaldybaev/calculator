package de.vwgis.calculator.service;

import de.vwgis.calculator.entity.CalcRequest;
import de.vwgis.calculator.entity.CalcResponse;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class CalculatorServiceTest {
    private static final CalculatorService service = new CalculatorService();
    private static final Logger LOG = LoggerFactory.getLogger(CalculatorServiceTest.class);

    @BeforeAll
    public static void setup() {
        LOG.info("init calculations result cache");
        initCache();
    }

    @Test
    public void test_whenCreateCalculationRequest_withValidParams_thenReturnCalculationResult() {
        CalcRequest request = new CalcRequest("10+10+15");
        request.setBuilderId("1");

        CalcResponse response = service.getResponse(request);

        assertEquals("341035470", response.getId());
        assertEquals("10+10+15", response.getExpression());
        assertEquals("35,00", response.getResult());
    }

    @Test
    public void test_whenGetAllCalculationRequests_withValidParams_thenReturnListOfCalculationResults() {
        String expected = "[CalcResponse(id=582101693, result=2, expression=(2+5*2)/(9/3)-2, error=), CalcResponse(id=510239828, result=6,00, expression=2*2+(9/3)-1, error=), CalcResponse(id=363057937, result=2,00, expression=((2+5*2)/(9/3))-2, error=), CalcResponse(id=510239797, result=5,00, expression=2*2+(9/3)-2, error=), CalcResponse(id=341035470, result=35,00, expression=10+10+15, error=)]";
        var actual = service.getAll();
        assertEquals(expected, actual.toString());
        assertEquals(5, actual.size());
    }

    @Test
    public void test_whenGetCalculationResultById_withValidParams_thenReturnCalculationResult() {
        String expected = "CalcResponse(id=582101693, result=2, expression=(2+5*2)/(9/3)-2, error=)";
        var actual = service.getResponseByKey("582101693");
        assertEquals(expected, actual.toString());
    }

    @Test
    public void test_whenUpdateCalculationResultById_withValidParams_thenReturnCalculationResult() {
        String id = "582101693";
        boolean beforeUpdate = service.isValidKey(id);
        CalcRequest request = new CalcRequest("(2+5*2)/(9*3)-2");
        request.setBuilderId("2");
        CalcResponse before = service.update(id, request);

        assertNull(service.getResponseByKey(id));

        boolean afterUpdate = service.isValidKey(id);

        assertEquals("-1,56", before.getResult());
        assertEquals("438955937", before.getId());
        assertEquals("(2+5*2)/(9*3)-2", before.getExpression());
        assertNotEquals(beforeUpdate, afterUpdate);
    }

    @Test
    public void test_whenDeleteCalculationResultById_withValidParams_thenReturnNoContent() {
        var before = service.getAll().size();
        String key = "510239797";
        var beforeDeleting = service.isValidKey(key);

        assertNotNull(service.getResponseByKey(key));

        service.delete(key);

        var after = service.getAll().size();
        var afterDeleting = service.isValidKey(key);

        assertNull(service.getResponseByKey(key));
        assertNotEquals(before, after);
        assertNotEquals(beforeDeleting, afterDeleting);
    }

    @Test
    public void test_whenDeleteCalculationResultById_withIncorrectParams_thenReturnNoContent() {
        var before = service.getAll().size();
        String key = "123456789";
        var beforeDeleting = service.isValidKey(key);

        assertNull(service.getResponseByKey(key));

        service.delete(key);

        var after = service.getAll().size();
        var afterDeleting = service.isValidKey(key);

        assertNull(service.getResponseByKey(key));
        assertEquals(before, after);
        assertEquals(beforeDeleting, afterDeleting);
    }

    @Test
    public void test_whenCreateCalculationRequest_withIncorrectLettersParams_thenReturnEmptyContent() {
        CalcRequest request = new CalcRequest("A+A+KK");
        request.setBuilderId("1");

        var beforeSize = service.getAll().size();

        service.add(request);

        var afterSize = service.getAll().size();

        assertEquals(beforeSize, afterSize);
    }

    @Test
    public void test_whenCreateCalculationRequest_withIncorrectSymbolParams_thenReturnEmptyContent() {
        CalcRequest request = new CalcRequest("¢+$+@");
        request.setBuilderId("1");

        var beforeSize = service.getAll().size();

        service.add(request);

        var afterSize = service.getAll().size();

        assertEquals(beforeSize, afterSize);
    }

    @Test
    public void test_whenCreateCalculationRequest_withNoParams_thenReturnEmptyContent() {
        CalcRequest request = new CalcRequest("");
        request.setBuilderId("");

        CalcResponse response = service.getResponse(request);

        String expected = "CalcResponse(id=0, result=0.0, expression=, error=)";

        assertEquals(expected, response.toString());
        assertEquals("0.0", response.getResult());
        assertEquals("", response.getExpression());
    }

    @Test
    public void test_whenCreateCalculationRequest_withIncorrectNoParams_thenReturnEmptyContent() {
        CalcRequest request = new CalcRequest("-+");
        request.setBuilderId("1");

        var beforeSize = service.getAll().size();

        service.add(request);

        var afterSize = service.getAll().size();

        assertEquals(beforeSize, afterSize);
    }

    @Test
    public void test_whenCreateCalculationRequest_withSpaceAsParam_thenReturnEmptyContent() {
        CalcRequest request = new CalcRequest("    ");
        request.setBuilderId("     ");

        var beforeSize = service.getAll().size();

        service.add(request);

        var afterSize = service.getAll().size();

        assertEquals(beforeSize, afterSize);
    }

    @Test
    public void test_whenCreateCalculationRequest_withIncorrectParam_thenReturnEmptyContent() {
        CalcRequest request = new CalcRequest("1+1)");
        request.setBuilderId("1");

        var beforeSize = service.getAll().size();

        service.add(request);

        var afterSize = service.getAll().size();

        assertEquals(beforeSize, afterSize);
    }

    private static void initCache() {
        CalcRequest request1 = new CalcRequest("1+3+2*2");
        request1.setBuilderId("1");
        service.add(request1);

        CalcRequest request2 = new CalcRequest("8/2+1");
        request2.setBuilderId("1");
        service.add(request2);

        CalcRequest request3 = new CalcRequest("7-1+1*5");
        request3.setBuilderId("1");
        service.add(request3);

        CalcRequest request4 = new CalcRequest("2*2+(9/3)-1");
        request4.setBuilderId("2");
        service.add(request4);

        CalcRequest request5 = new CalcRequest("((2+5*2)/(9/3))-2");
        request5.setBuilderId("2");
        service.add(request5);

        CalcRequest request6 = new CalcRequest("2*2+(9/3)-2");
        request6.setBuilderId("2");
        service.add(request6);

        CalcRequest request7 = new CalcRequest("(2+5*2)/(9/3)-2");
        request7.setBuilderId("3");
        service.add(request7);
    }
}