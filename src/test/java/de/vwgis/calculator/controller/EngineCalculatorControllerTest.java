package de.vwgis.calculator.controller;

import de.vwgis.calculator.entity.CalcRequest;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

import static de.vwgis.calculator.util.WebUtil.generateId;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EngineCalculatorControllerTest {
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void test_whenGetCalculationResultById_withValidParams_thenReturnCalculationResult() {
        URI targetUrl = UriComponentsBuilder.fromUriString("/v3/calculate/582101693").build().encode().toUri();
        String message = restTemplate.getForObject(targetUrl, String.class);
        String expected = "{\"id\":\"582101693\",\"result\":\"2\",\"expression\":\"(2+5*2)/(9/3)-2\",\"error\":\"\"}";
        Assert.assertEquals(expected, message);
    }

    @Test
    public void test_whenDeleteCalculationResultById_withCorrectParams_thenReturnNoContent() {
        String expression = "(5*5)/5";
        CalcRequest request = new CalcRequest(expression);
        request.setBuilderId("3");

        ResponseEntity<String> r = createCalculationRequest("{\"expression\": \"" + expression + "\"}");
        assertEquals(HttpStatus.OK, r.getStatusCode());

        String id = generateId(request);

        ResponseEntity<Void> response = restTemplate.exchange("/v3/calculate/" + id, HttpMethod.DELETE, null, Void.class, 1);

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void test_whenDeleteCalculationResultById_withIncorrectParams_thenReturnNoContent() {
        String id = "164817134555";
        ResponseEntity<Void> response = restTemplate.exchange("/v3/calculate/" + id, HttpMethod.DELETE, null, Void.class, 1);

        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void test_whenGetAllCalculationRequests_withValidParams_thenReturnListOfCalculationResults() {
        URI targetUrl = UriComponentsBuilder.fromUriString("/v3/calculate/").build().encode().toUri();
        String message = restTemplate.getForObject(targetUrl, String.class);
        /*
        // if you run only this class, then this result will be
        String expected = "[{\"id\":\"582101693\",\"result\":\"2\",\"expression\":\"(2+5*2)/(9/3)-2\",\"error\":\"\"}]";*/
        // if you run all test classes, then this result will be
        String expected = "[{\"id\":\"582101693\",\"result\":\"2\",\"expression\":\"(2+5*2)/(9/3)-2\",\"error\":\"\"},{\"id\":\"1319804646\",\"result\":\"3,33\",\"expression\":\"(5*2)/(9/3)\",\"error\":\"\"}]";
        Assert.assertEquals(expected, message);
    }

    @Test
    public void test_whenCreateCalculationRequest_withValidParams_thenReturnCalculationResult() {
        ResponseEntity<String> response = createCalculationRequest("{\"expression\": \"(2+5*2)/(9/3)-2\"}");
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void test_whenUpdateCalculationRequest_withValidParams_thenReturnCalculationResult() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        String url = "/v3/calculate/582101693";
        String updateExpression = "{\"expression\": \"(5*2)/(9/3)\"}";
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<>(updateExpression, headers), String.class);

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void test_whenCreateCalculationRequest_withIncorrectLettersParams_thenReturnCalculationResult() {
        var response = createCalculationRequest("A+1A+KK");
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void test_whenCreateCalculationRequest_withIncorrectSymbolParams_thenReturnCalculationResult() {
        var response = createCalculationRequest("¢+$+@");

        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void test_whenCreateCalculationRequest_withNoParams_thenReturnEmptyContent() {
        var response = createCalculationRequest("");

        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void test_whenCreateCalculationRequest_withIncorrectNoParams_thenReturnEmptyContent() {
        var response = createCalculationRequest("-+");
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void test_whenCreateCalculationRequest_withIncorrectParams_thenReturnEmptyContent() {
        var response = createCalculationRequest("1+1)");
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    private ResponseEntity<String> createCalculationRequest(String expression) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return restTemplate.exchange("/v3/calculate/", HttpMethod.POST, new HttpEntity<>(expression, headers), String.class);
    }
}