package de.vwgis.calculator.builder;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AdvancedCalculatorTest {
    private final ICalculator calculator = new AdvancedCalculator();

    @Test
    void test_whenBasicCalculationRequest_withCorrectParams_thenReturnCalculationResult() {
        assertEquals("2,00", calculator.calculate("1+1"));
        assertEquals("1,00", calculator.calculate("3-2"));
        assertEquals("8,00", calculator.calculate("4*2"));
        assertEquals("2,00", calculator.calculate("8/4"));
        assertEquals("1,33", calculator.calculate("4/3"));
        assertEquals("-4,00", calculator.calculate("-8+4"));
    }

    @Test
    void test_whenComplexCalculationRequest_withCorrectParams_thenReturnCalculationResult() {
        assertEquals("12,00", calculator.calculate("1+1+1+9"));
        assertEquals("13,00", calculator.calculate("2*8-9/3"));
        assertEquals("17,00", calculator.calculate("4*2+9"));
        assertEquals("1,00", calculator.calculate("8/4-1"));
    }

    @Test
    void test_whenAdvancedCalculationRequest_withCorrectParams_thenReturnCalculationResult() {
        assertEquals("12,00", calculator.calculate("(1+1)*3+(12/2)"));
        assertEquals("25,00", calculator.calculate("5*(7+3)/2"));
        assertEquals("14,00", calculator.calculate("5+((1+2)*4)-3"));
        assertEquals("18,51", calculator.calculate("5.4+((1.9+2.2)*4.1)-3.7"));
    }
}