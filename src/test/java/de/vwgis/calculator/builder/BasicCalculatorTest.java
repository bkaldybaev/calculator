package de.vwgis.calculator.builder;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BasicCalculatorTest {

    private final ICalculator calculator = new BasicCalculator();

    @Test
    void test_whenBasicCalculationRequest_withCorrectParams_thenReturnCalculationResult() {
        assertEquals("2,00", calculator.calculate("1+1"));
        assertEquals("3,40", calculator.calculate("1.5+1.9"));
        assertEquals("1,00", calculator.calculate("3-2"));
        assertEquals("8,00", calculator.calculate("4*2"));
        assertEquals("2,00", calculator.calculate("8/4"));
        assertEquals("1,33", calculator.calculate("4/3"));
        assertEquals("-4,00", calculator.calculate("-8+4"));
        assertEquals("-3,20", calculator.calculate("-8.1+4.9"));
        assertEquals("-3,14", calculator.calculate("-8.111111111111+4.974365746378564389"));
    }

    @Test
    void test_whenComplexCalculationRequest_withCorrectParams_thenReturnCalculationResult() {
        assertEquals("12,00", calculator.calculate("1+1+1+9"));
        assertEquals("13,00", calculator.calculate("2*8-9/3"));
        assertEquals("17,00", calculator.calculate("4*2+9"));
        assertEquals("1,00", calculator.calculate("8/4-1"));
        assertEquals("0,78", calculator.calculate("8.9/4.5-1.2"));
    }
}