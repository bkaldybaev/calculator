package de.vwgis.calculator.builder;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class EngineCalculatorTest {
    private final ICalculator calculator = new EngineCalculator();

    @Test
    void test_whenBasicCalculationRequest_withCorrectParams_thenReturnCalculationResult() {
        assertEquals("2.3", calculator.calculate("1.3+1"));
        assertEquals("1", calculator.calculate("3-2"));
        assertEquals("8", calculator.calculate("4*2"));
        assertEquals("2", calculator.calculate("8/4"));
        assertEquals("-4", calculator.calculate("-8+4"));
        assertEquals("1.3333333333333333", calculator.calculate("4/3"));
    }

    @Test
    void test_whenComplexCalculationRequest_withCorrectParams_thenReturnCalculationResult() {
        assertEquals("12", calculator.calculate("1+1+1+9"));
        assertEquals("13", calculator.calculate("2*8-9/3"));
        assertEquals("17", calculator.calculate("4*2+9"));
        assertEquals("1", calculator.calculate("8/4-1"));
    }

    @Test
    void test_whenAdvancedCalculationRequest_withCorrectParams_thenReturnCalculationResult() {
        assertEquals("12", calculator.calculate("(1+1)*3+(12/2)"));
        assertEquals("25", calculator.calculate("5*(7+3)/2"));
        assertEquals("14", calculator.calculate("5+((1+2)*4)-3"));
    }
}