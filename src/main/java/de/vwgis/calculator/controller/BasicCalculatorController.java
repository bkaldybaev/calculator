package de.vwgis.calculator.controller;

import de.vwgis.calculator.entity.CalcRequest;
import de.vwgis.calculator.entity.CalcResponse;
import de.vwgis.calculator.entity.Response;
import de.vwgis.calculator.service.CalculatorService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static de.vwgis.calculator.util.WebUtil.isBalancedBracket;

@RestController
@RequestMapping(value = "/v1/calculate")
public class BasicCalculatorController {

    @Autowired
    private CalculatorService service;

    @PostMapping("/")
    public ResponseEntity<?> create(@RequestBody @NotNull CalcRequest request) {
        request.setBuilderId("1");
        if (request.isValid() && !isBalancedBracket(request.getExpression())) {
            return ResponseEntity.ok(service.getResponse(request));
        } else {
            return new ResponseEntity<>(new Response("this expression " + request.getExpression() + " is incorrect"), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/")
    public ResponseEntity<?> getAllCalculationResults() {
        return ResponseEntity.ok(service.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getCalculationResult(@PathVariable("id") String id) {
        CalcResponse response = service.getResponseByKey(id);
        if (response == null) {
            return new ResponseEntity<>(new Response("this " + id + " is invalid."), HttpStatus.BAD_REQUEST);
        } else {
            return ResponseEntity.ok(response);
        }
    }

    @PutMapping("/{id}")
    ResponseEntity<?> replaceCalculation(@RequestBody CalcRequest request, @PathVariable("id") String id) {
        if (!service.isValidKey(id)) {
            return new ResponseEntity<>(new Response("this " + id + " is invalid."), HttpStatus.BAD_REQUEST);
        } else {
            request.setBuilderId("1");
            CalcResponse response = service.update(id, request);
            return ResponseEntity.ok(response);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCalculationResult(@PathVariable("id") String id) {
        if (!service.isValidKey(id)) {
            return new ResponseEntity<>(new Response("this " + id + " is invalid."), HttpStatus.BAD_REQUEST);
        } else {
            service.delete(id);
            return new ResponseEntity<>(new Response("Calculation " + id + " successfully deleted."), HttpStatus.OK);
        }
    }
}
