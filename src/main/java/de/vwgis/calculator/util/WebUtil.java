package de.vwgis.calculator.util;

import de.vwgis.calculator.entity.CalcRequest;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class WebUtil {

    /**
     * Generates a unique ID for the given calculation request based on the request's expression and builder ID.
     *
     * @param request the calculation request to generate the ID for
     * @return a unique ID for the given calculation request
     */
    public static @NotNull String generateId(@NotNull CalcRequest request) {
        return (request.getExpression().equals("")) ? "0" : String.valueOf(Math.abs(Objects.hash(request.getExpression(), request.getBuilderId())));
    }

    /**
     * Returns true if the given expression is a valid expression for the given builder ID, false otherwise.
     *
     * @param expression the expression to validate
     * @param builderId  the ID of the builder to use for validation
     * @return true if the given expression is a valid expression for the given builder ID, false otherwise
     */
    public static boolean isValidExpression(String expression, @NotNull String builderId) {
        boolean res;
        switch (builderId) {
            case "1" -> res = isValidBasicScript(expression);
            case "2", "3" -> res = isValidScript(expression);
            default -> res = false;
        }
        return res;
    }

    /**
     * Returns true if the given expression is a valid script (i.e., it contains at least one digit and one operator),
     * false otherwise.
     *
     * @param expression the expression to validate
     * @return true if the given expression is a valid script, false otherwise
     */
    public static boolean isValidScript(@NotNull String expression) {
        if (expression.length() < 3) {
            return false;
        }
        expression = expression.replaceAll("\\s", "");
        return expression.matches(".*\\d+.*") && expression.matches(".*[+\\-*/(). ].*");
    }

    /**
     * Returns true if the given expression is a valid basic script (i.e., it is surrounded by parentheses and contains
     * at least one digit and one operator), false otherwise.
     *
     * @param expression the expression to validate
     * @return true if the given expression is a valid basic script, false otherwise
     */
    public static boolean isBalancedBracket(@NotNull String expression) {
        return expression.matches("^\\(.*\\)$");
    }

    /**
     * Returns true if the given expression contains any brackets, false otherwise.
     *
     * @param expression the expression to check for brackets
     * @return true if the given expression contains any brackets, false otherwise
     */
    public static boolean containsBracket(@NotNull String expression) {
        return expression.matches(".*[+\\-*/(). ].*");
    }

    /**
     * Returns true if the given expression is a valid basic script (i.e., it contains at least one digit and one operator),
     * false otherwise.
     *
     * @param expression the expression to validate
     * @return true if the given expression is a valid basic script, false otherwise
     */
    public static boolean isValidBasicScript(@NotNull String expression) {
        if (expression.length() < 3) {
            return false;
        }
        expression = expression.replaceAll("\\s", "");
        return expression.matches(".*\\d+.*") && expression.matches(".*[+\\-*/].*");
    }
}
