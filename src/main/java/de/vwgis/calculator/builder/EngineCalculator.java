package de.vwgis.calculator.builder;

import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Engine;
import org.jetbrains.annotations.NotNull;

/**
 * EngineCalculator is a Java class that uses the Graal.js JavaScript engine to evaluate JavaScript expressions.
 * <p>
 * The Engine object is created using the Engine.newBuilder() method, which allows you to configure options for the engine.
 * In this case, the option "engine.WarnInterpreterOnly" is set to "false" to disable warning messages about interpreter-only features.
 * <p>
 * The Context object is created using the Context.newBuilder() method, which allows you to specify the language and
 * engine to use for the context. In this case, the language is "js" (JavaScript) and the engine is the Graal.js engine that was created earlier.
 * <p>
 * The "calculate()" method takes a String containing a JavaScript expression as an argument, and uses the eval() method
 * of the Context object to evaluate the expression. The result of the evaluation is returned as a String.
 * <p>
 * For example, if you call calculate("2+3"), the method will return the String "5".
 */
public class EngineCalculator implements ICalculator {
    private static final Engine engine = Engine.newBuilder().option("engine.WarnInterpreterOnly", "false").build();
    private static final Context ctx = Context.newBuilder("js").engine(engine).build();

    @Override
    public String calculate(@NotNull String expression) {
        return ctx.eval("js", expression).toString();
    }
}
