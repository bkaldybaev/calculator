package de.vwgis.calculator.builder;

import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 * An advanced calculator which can parse and evaluate a mathematical expression written in infix notation.
 * The calculator can handle basic arithmetic operations such as addition, subtraction, multiplication, and division,
 * as well as brackets.
 * <p>
 * Complexity: Time = O(n), Space = O(n)
 */
public class AdvancedCalculator implements ICalculator {
    private static final String OPERATORS = "+-*/";
    private static final char OPENING_BRACKET = '(';
    private static final char CLOSING_BRACKET = ')';

    /**
     * Evaluates the given mathematical expression and returns the result as a string.
     *
     * @param expression The mathematical expression to be evaluated.
     * @return The result of the calculation, rounded to two decimal places and converted to a string.
     */
    @Override
    public String calculate(@NotNull String expression) {
        Queue<Character> expressionInQueue = new LinkedList<>();

        for (char c : expression.toCharArray()) {
            expressionInQueue.offer(c);
        }

        return getResInString(evaluate(expressionInQueue));
    }

    /**
     * Converts the given double value to a string, rounding it to two decimal places.
     *
     * @param res The double value to be converted.
     * @return The rounded string representation of the double value.
     */
    private static @NotNull String getResInString(double res) {
        return String.format("%.2f", res);
    }

    /**
     * Evaluates the mathematical expression contained in the given queue of characters.
     *
     * @param expressionInQueue The queue containing the characters of the mathematical expression.
     * @return The result of the calculation.
     */
    private double evaluate(@NotNull Queue<Character> expressionInQueue) {
        double number = 0.0;
        Stack<Double> stack = new Stack<>();
        char operator = '+';
        boolean decimal = false;
        int decimalPlaces = 1;

        while (!expressionInQueue.isEmpty()) {
            char elem = expressionInQueue.poll();
            if (Character.isDigit(elem)) {
                if (decimal) {
                    number += (elem - '0') / Math.pow(10, decimalPlaces);
                    decimalPlaces++;
                } else {
                    number = 10.0 * number + elem - '0';
                }
            } else if (elem == '.') {
                decimal = true;
            } else if (elem == OPENING_BRACKET) {
                number = evaluate(expressionInQueue);
            } else if (OPERATORS.indexOf(elem) != -1) {
                operatorFunction(stack, number, operator);
                number = 0;
                operator = elem;
                decimal = false;
                decimalPlaces = 1;
            } else if (elem == CLOSING_BRACKET) {
                break;
            }
        }
        operatorFunction(stack, number, operator);

        return stack.stream().mapToDouble(a -> a).sum();
    }

    /**
     * Performs the operation specified by the given operator on the given number and the top element of the given stack.
     * The result is then pushed back onto the stack.
     *
     * @param stack    The stack containing the intermediate results.
     * @param number   The second operand of the operation.
     * @param operator The operator to be applied.
     */
    private void operatorFunction(Stack<Double> stack, double number, char operator) {
        switch (operator) {
            case '+' -> stack.push(number);
            case '-' -> stack.push(-number);
            case '*' -> stack.push(stack.pop() * number);
            case '/' -> stack.push(stack.pop() / number);
        }
    }
}
