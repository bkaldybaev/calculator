package de.vwgis.calculator.builder;

import org.jetbrains.annotations.NotNull;

public interface ICalculator {
    String calculate(@NotNull String expression);
}
