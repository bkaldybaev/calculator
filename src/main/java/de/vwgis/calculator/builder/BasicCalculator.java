package de.vwgis.calculator.builder;

import org.jetbrains.annotations.NotNull;

import java.util.Stack;


/**
 * A basic calculator that can parse and evaluate simple mathematical expressions.
 * Expressions are expected to be in infix notation and use the +, -, *, and / operators.
 * <p>
 * Complexity: Time = O(n), Space = O(n)
 */
public class BasicCalculator implements ICalculator {
    private static final String OPERATORS = "+-*/";

    /**
     * Calculates the result of the given mathematical expression.
     *
     * @param expression a string representation of the expression to evaluate
     * @return a string representation of the result of the calculation
     */
    @Override
    public String calculate(@NotNull String expression) {
        double number = 0.0;
        Stack<Double> stack = new Stack<>();
        char operator = '+';
        boolean decimal = false;
        int decimalPlaces = 1;

        for (char elem : expression.toCharArray()) {
            if (Character.isDigit(elem)) {
                if (decimal) {
                    number += (elem - '0') / Math.pow(10, decimalPlaces);
                    decimalPlaces++;
                } else {
                    number = 10.0 * number + elem - '0';
                }
            } else if (elem == '.') {
                decimal = true;
            } else if (OPERATORS.indexOf(elem) != -1) {
                operatorFunction(stack, number, operator);
                number = 0.0;
                operator = elem;
                decimal = false;
                decimalPlaces = 1;
            }
        }
        operatorFunction(stack, number, operator);
        return getResInString(stack);
    }

    /**
     * Returns the result of the calculation as a string, rounded to two decimal places.
     *
     * @param stack a stack containing the intermediate results of the calculation
     * @return the result of the calculation as a string
     */
    private static @NotNull String getResInString(@NotNull Stack<Double> stack) {
        return String.format("%.2f", stack.stream().mapToDouble(a -> a).sum());
    }

    /**
     * Performs the operation specified by the given operator on the given number and the top value on the stack.
     *
     * @param stack    a stack containing the intermediate results of the calculation
     * @param number   the current number being parsed
     * @param operator the current operator
     */
    private void operatorFunction(Stack<Double> stack, double number, char operator) {
        switch (operator) {
            case '*' -> stack.push(stack.pop() * number);
            case '/' -> stack.push(stack.pop() / number);
            case '+' -> stack.push(number);
            case '-' -> stack.push(-number);
        }
    }
}
