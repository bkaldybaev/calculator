package de.vwgis.calculator.service;

import de.vwgis.calculator.builder.AdvancedCalculator;
import de.vwgis.calculator.builder.BasicCalculator;
import de.vwgis.calculator.builder.EngineCalculator;
import de.vwgis.calculator.entity.CalcRequest;
import de.vwgis.calculator.entity.CalcResponse;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static de.vwgis.calculator.util.WebUtil.generateId;
import static de.vwgis.calculator.util.WebUtil.isBalancedBracket;

/**
 * A service that provides a set of calculator objects for performing various types of calculations,
 * and stores the results of those calculations in a cache for later retrieval.
 */
@Service
public class CalculatorService {

    private static final Logger LOG = LoggerFactory.getLogger(CalculatorService.class);
    private final Map<String, CalcResponse> responseCache = new ConcurrentHashMap<>();

    /**
     * Returns a list of all the calculation responses stored in the cache.
     *
     * @return a list of all the calculation responses stored in the cache
     */
    public List<CalcResponse> getAll() {
        return new ArrayList<>(responseCache.values());
    }

    /**
     * Returns the calculation response with the given key from the cache, or a new empty response if the key is invalid.
     *
     * @param key the key of the calculation response to retrieve
     * @return the calculation response with the given key, or a new empty response if the key is invalid
     */
    public CalcResponse getResponseByKey(String key) {
        if (key != null && !key.equals("")) {
            return responseCache.get(key);
        } else {
            LOG.warn("Incorrect Key or empty");
            return new CalcResponse();
        }
    }

    /**
     * Adds the given calculation request to the cache, using the request's ID as the key.
     *
     * @param request the calculation request to add to the cache
     */
    public void add(@NotNull CalcRequest request) {
        if (request.getBuilderId().equals("1") && !isBalancedBracket(request.getExpression())) {
            LOG.warn("incorrect basic calculation request");
            return;
        }

        if (!request.isValid()) {
            LOG.warn("Incorrect request");
            return;
        }

        CalcResponse response = new CalcResponse(request);
        response.setResult(calculate(request));
        responseCache.put(response.getId(), response);
    }

    /**
     * Removes the calculation response with the given key from the cache.
     *
     * @param key the key of the calculation response to remove
     */
    public void delete(String key) {
        if (key != null && !key.equals("")) {
            responseCache.entrySet().removeIf(entry -> entry.getKey().equals(key));
        } else {
            LOG.warn("Incorrect Key or empty");
        }
    }

    /**
     * Returns the calculation response for the given calculation request, either by retrieving it from the cache
     * or by calculating it and adding it to the cache.
     *
     * @param calcRequest the calculation request to get the response for
     * @return the calculation response for the given calculation request
     */
    public CalcResponse getResponse(@NotNull CalcRequest calcRequest) {
        String key = generateId(calcRequest);
        CalcResponse response = responseCache.get(key);

        if (response == null) {
            response = new CalcResponse(calcRequest);
            response.setResult(calculate(calcRequest));
            responseCache.put(response.getId(), response);
        }

        return response;
    }

    /**
     * Calculates the result of the given calculation request.
     *
     * @param calcRequest the calculation request to calculate the result for
     * @return the result of the calculation
     */
    private String calculate(@NotNull CalcRequest calcRequest) {
        String res = "0.0";
        switch (calcRequest.getBuilderId()) {
            case "1" -> res = new BasicCalculator().calculate(calcRequest.getExpression());
            case "2" -> res = new AdvancedCalculator().calculate(calcRequest.getExpression());
            case "3" -> res = new EngineCalculator().calculate(calcRequest.getExpression());
        }
        return res;
    }

    /**
     * Returns true if the cache contains a calculation response with the given ID, false otherwise.
     *
     * @param id the ID of the calculation response to search for
     * @return true if the cache contains a calculation response with the given ID, false otherwise
     */
    public boolean isValidKey(String id) {
        return responseCache.containsKey(id);
    }


    /**
     * Returns the computed response for the given compute request, evaluating it and adding it to the cache.
     *
     * @param id      the ID needed to delete the result
     * @param request the calculation request to evaluating and adding it to the cache
     * @return the calculation response for the given calculation request
     */
    public CalcResponse update(String id, CalcRequest request) {
        responseCache.remove(id);

        CalcResponse response = new CalcResponse(request);
        response.setResult(calculate(request));
        responseCache.put(response.getId(), response);

        return response;
    }
}
