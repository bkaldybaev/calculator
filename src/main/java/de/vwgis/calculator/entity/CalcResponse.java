package de.vwgis.calculator.entity;

import lombok.Data;
import org.jetbrains.annotations.NotNull;

import static de.vwgis.calculator.util.WebUtil.generateId;

@Data
public class CalcResponse {
    private String id;
    private String result;
    private String expression;
    private String error;

    public CalcResponse(@NotNull CalcRequest request) {
        this.id = generateId(request);
        this.result = "0.0";
        this.expression = request.getExpression();
        this.error = "";
    }

    public CalcResponse() {
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
