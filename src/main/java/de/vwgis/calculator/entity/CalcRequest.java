package de.vwgis.calculator.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import static de.vwgis.calculator.util.WebUtil.isValidExpression;

@Data
public class CalcRequest {
    private String expression;
    private String builderId;

    public CalcRequest(@JsonProperty("expression") String expression) {
        this.expression = expression;
    }

    public boolean isValid() {
        return isValidExpression(this.expression, this.builderId);
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return "{ expression='" + expression + "'}";
    }
}
